/*
 *                                                                            
 *  Copyright (c) 2011 - 2016 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
 package org.eclipse.osbp.abstractstatemachine;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LCompare;
import org.eclipse.osbp.jpa.services.filters.LIsNull;
import org.eclipse.osbp.jpa.services.filters.LLike;
import org.eclipse.osbp.jpa.services.filters.LNot;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.filter.IQuery;
import org.eclipse.osbp.ui.api.statemachine.IDataProvider;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;

@SuppressWarnings("restriction")
public abstract class AbstractDataProvider extends AbstractStateMachineParticipant
		implements IDataProvider, IDataProvider.Provider {

	protected Map<String, IDTOService<? extends IDto>> dtoServices = new HashMap<>();
	protected boolean ready;

	@Override
	public void setStatemachine(IStateMachine statemachine) {
		super.setStatemachine(statemachine);
		statemachine.registerDataProvider(this);
	}

	@Override 
	public void addDatasource(String dtoAlias, Class<?> dtoServiceClass) {
		@SuppressWarnings("unchecked")
		Class<IDto> type = (Class<IDto>)dtoServiceClass;
		dtoServices.put(dtoAlias, DtoServiceAccess.getService(type));
	}

	@Override
	public Boolean find(IDataProvider.Provider listener, String dtoAlias, String fieldName, Object search) {
		Boolean retval = find(listener, dtoAlias, new Query(new LCompare.Equal(fieldName, search)));
		if(!retval) {
			LOGGER.error("find for {} failed", search);
		}
		return retval;
	}

	@Override
	public Boolean find(IDataProvider.Provider listener, String dtoAlias, IQuery query) {
		Collection<? extends IDto> results = findDtoInstances(dtoAlias, query);
		if (results.size() > 1) {
			LOGGER.error("find for {} returned multiple values - first value is used", dtoAlias);
		}
		if (results.isEmpty()) {
			LOGGER.error("find for {} returned no values", dtoAlias);
		}
		try {
			Method method = setter(listener, dtoAlias, IDto.class, 0);
			if (method == null) {
				return false;
			}
			if (results.isEmpty()) {
				method.invoke(listener, (IDto) null);
				return false;
			} else {
				method.invoke(listener, (IDto) results.iterator().next());
			}
			return true;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOGGER.error("{}", e);
			return false;
		}
	}

	@Override
	public Collection<? extends IDto> findAll(String dtoAlias, IQuery query) {
		return findDtoInstances(dtoAlias, query);
	}

	@Override
	public Collection<? extends IDto> findAll(String dtoAlias, String fieldName, Object search) {
		if(search instanceof String) {
			if(((String)search).equalsIgnoreCase("[isnotnull]")) {
				return findDtoInstances(dtoAlias, new Query(new LNot(new LIsNull(fieldName))));
			}
			if(((String)search).equalsIgnoreCase("[isnull]")) {
				return findDtoInstances(dtoAlias, new Query(new LIsNull(fieldName)));
			}
			if(((String)search).contains("%") || ((String)search).contains("_")) {
				return findDtoInstances(dtoAlias, new Query(new LLike(fieldName, (String) search)));
			}
		}
		return findDtoInstances(dtoAlias, new Query(new LCompare.Equal(fieldName, search)));
	}

	@Override
	public Collection<? extends IDto> findAll(String dtoAlias, String fieldName) {
		return findDtoInstances(dtoAlias, new Query());
	}

	protected Collection<? extends IDto> findDtoInstances(String dtoAlias, IQuery query) {
		if (!dtoServices.containsKey(dtoAlias)) {
			LOGGER.error("{} was not bound from ui model", dtoAlias);
			return Collections.emptyList();
		}
		IDTOService<? extends IDto> dtoService = dtoServices.get(dtoAlias);
		if (dtoService == null) {
			LOGGER.error("{} has no service registrated", dtoAlias);
			return Collections.emptyList();
		}
		try {
			return dtoService.find(query);
		} catch (Exception e) {
			LOGGER.error("{}", e);
		}
		return Collections.emptyList();
	}

	protected Method dispatchMethod(Provider listener, String methodName) {
		try {
			Class<?> clz = listener.getClass();
			BeanInfo info = Introspector.getBeanInfo(clz);
			for (MethodDescriptor desc : info.getMethodDescriptors()) {
				if (desc.getName().equalsIgnoreCase(methodName)) {
					return desc.getMethod();
				}
			}
		} catch (IllegalArgumentException | IntrospectionException e) {
			LOGGER.error("{}", e);
		}
		return null;
	}

	@Override
	public void addTo(Provider listener, String dtoAlias, String listName, IDto dto) {
		Method method = dispatchMethod(listener, "addTo" + dtoAlias + listName);
		if (method != null) {
			try {
				method.invoke(listener, dto);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("{}", e);
			}
		} else {
			LOGGER.error("method addTo%s%s not generated", dtoAlias, listName);
		}
	}

	@Override
	public void removeFrom(Provider listener, String dtoAlias, String listName, IDto dto) {
		Method method = dispatchMethod(listener, "removeFrom" + dtoAlias + listName);
		if (method != null) {
			try {
				method.invoke(listener, dto);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("{}", e);
			}
		} else {
			LOGGER.error("method removeFrom%s%s not generated", dtoAlias, listName);
		}
	}

	@Override
	public void update(Provider listener, String dtoAlias, IDto dto) {
		Method method = dispatchMethod(listener, "update" + dtoAlias);
		if (method != null) {
			try {
				method.invoke(listener, dto);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("{}", e);
			}
		} else {
			LOGGER.error("method update%s not generated", dtoAlias);
		}
	}

	@Override
	public void reload(Provider listener, String dtoAlias, IDto dto) {
		Method method = dispatchMethod(listener, "reload" + dtoAlias);
		if (method != null) {
			try {
				method.invoke(listener, dto);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("{}", e);
			}
		} else {
			LOGGER.error("method reload%s not generated", dtoAlias);
		}
	}

	@Override
	public void delete(Provider listener, String dtoAlias, IDto dto) {
		Method method = dispatchMethod(listener, "delete" + dtoAlias);
		if (method != null) {
			try {
				method.invoke(listener, dto);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				LOGGER.error("{}", e);
			}
		} else {
			LOGGER.error("delete update%s not generated", dtoAlias);
		}
	}
}
